//HUONG DAN GUI-NHAN o DUOI CUNG

// SPI(nRF24L01) commands
#define READ_REG        0x00  // Define read command to register
#define WRITE_REG       0x20  // Define write command to register
#define RD_RX_PLOAD     0x61  // Define RX payload register address
#define WR_TX_PLOAD     0xA0  // Define TX payload register address
#define FLUSH_TX        0xE1  // Define flush TX register command
#define FLUSH_RX        0xE2  // Define flush RX register command
#define REUSE_TX_PL     0xE3  // Define reuse TX payload register command
#define NOP             0xFF  // Define No Operation, might be used to read status register
//------------------------
#define RX_DR    0x40
#define TX_DS    0x20
#define MAX_RT   0x10
//-------------------------

#define CONFIG          0x00  // 'Config' register address
#define EN_AA           0x01  // 'Enable Auto Acknowledgment' register address
#define EN_RXADDR       0x02  // 'Enabled RX addresses' register address
#define SETUP_AW        0x03  // 'Setup address width' register address
#define SETUP_RETR      0x04  // 'Setup Auto. Retrans' register address
#define RF_CH           0x05  // 'RF channel' register address
#define RF_SETUP        0x06  // 'RF setup' register address
#define STATUS          0x07  // 'Status' register address
#define OBSERVE_TX      0x08  // 'Observe TX' register address
#define RPD              0x09  // 'Carrier Detect' register address
#define RX_ADDR_P0      0x0A  // 'RX address pipe0' register address
#define RX_ADDR_P1      0x0B  // 'RX address pipe1' register address
#define RX_ADDR_P2      0x0C  // 'RX address pipe2' register address
#define RX_ADDR_P3      0x0D  // 'RX address pipe3' register address
#define RX_ADDR_P4      0x0E  // 'RX address pipe4' register address
#define RX_ADDR_P5      0x0F  // 'RX address pipe5' register address
#define TX_ADDR         0x10  // 'TX address' register address
#define RX_PW_P0        0x11  // 'RX payload width, pipe0' register address
#define RX_PW_P1        0x12  // 'RX payload width, pipe1' register address
#define RX_PW_P2        0x13  // 'RX payload width, pipe2' register address
#define RX_PW_P3        0x14  // 'RX payload width, pipe3' register address
#define RX_PW_P4        0x15  // 'RX payload width, pipe4' register address
#define RX_PW_P5        0x16  // 'RX payload width, pipe5' register address
#define FIFO_STATUS     0x17  // 'FIFO Status Register' register address
#define FEATURE         0x1D  // 'FEATURE' register address
#define DYNPD           0x1C  // 'DYNAMIC PAYLOAD' register address  
/*----------------------
Moi dia chi truyen-nhan gom 5 bytes
Che do Multireceive thi 1 PRX co the nhan duoc cung luc tu 6 PTX khac nhau
Du lieu se trao doi qua cac Pipe, tu Pipe_0 den Pipe_5
Tu Pipe_2 tro di, dia chi nhan se co 4 byte dau giong dia chi cua Pipe_1

O ben GUI
PTX_0: 5byte (Address1)
PTX_1: 5byte (Address2)
PTX_2: 4byte (Address2) + 1 byte (Address3)
PTX_3: 4byte (Address2) + 1 byte (Address4)
PTX_4: 4byte (Address2) + 1 byte (Address5)
PTX_5: 4byte (Address2) + 1 byte (Address6)

O ben NHAN do chi co 1 con nen ta quan tam den dia chi cac pipe nhan khac nhau
Pipe0_RX_Add: 5byte (Address1)
Pipe1_RX_Add: 5byte (Address2)
Pipe2_RX_Add: 4byte (Address2) + 1 byte (Address3)
Pipe3_RX_Add: 4byte (Address2) + 1 byte (Address4)
Pipe4_RX_Add: 4byte (Address2) + 1 byte (Address5)
Pipe5_RX_Add: 4byte (Address2) + 1 byte (Address6)

----------------------*/
// unsigned char Send_Add, Address1, Address2, Address3, Address4, Address5, Address6;
unsigned char Address1, Address2, Address3, Address4;

/*-------SPI---------*/
void SPI_Write(unsigned char Buff);
unsigned char SPI_Read(void);
void RF_Command(unsigned char command);
void RF_Write(unsigned char Reg_Add, unsigned char Value);
void RF_Write_Add(unsigned char Reg_Add, unsigned char Value);
void Common_Config();
/*-------TX_Mode---------*/
/*
void RF_Write_Add_TX_2(unsigned char Reg_Add, unsigned char Address, unsigned char Address2);
void TX_Address(unsigned char Address);
void TX_Address_2(unsigned char Address, unsigned char Address2);
void TX_Mode();
void TX_Config();
void TX_Config_2();
void TX_Send();
void TX_Send_2();
*/
/*-------RX_Mode---------*/
void RF_Write_Add_RX_2(unsigned char Reg_Add, unsigned char Address, unsigned char Address2);
void RX_Address(unsigned char Address_Pipe, unsigned char Address);
void RX_Address_2(unsigned char Address_Pipe, unsigned char Address, unsigned char Address2);
void RX_Mode();
//void RX_Config();
void RX_Config_4();
//void RX_Config_6();
void RX_Read();



/*----------------------
Cau truc du lieu dang typedef se chuyen du lieu struct trong mang station_info voi 
cac thuoc tinh cua struct ben trong va gan vao station_receiver cho ben nhan hoac
station_send cho ben gui.
----------------------*/
typedef struct{
    int flag;
    int light;
    int humi;
    int temp;
    int sm;  
    int water;
}station_info;

station_info station_receive;
// station_info station_send;
/*----------------------
Day la ham ghi du lieu vao SPI 
Do khong dung module SPI cung tren cac chan I/O cua AVR
Nen su dung ham nay de ghi du lieu vao
Dong thoi doc gia tri ma SPI tro ve, tham vong check duoc ACK
----------------------*/
void SPI_Write(unsigned char Buff){
    unsigned char bit_ctr;
    for(bit_ctr=0;bit_ctr<8;bit_ctr++){
        MOSI = (Buff & 0x80);         
        delay_us(5);
        Buff = (Buff << 1);           
        SCK = 1;                      
        delay_us(5);
        Buff |= MISO;           
        SCK = 0;
    }
}
/*----------------------
Day la ham doc du lieu ra tu SPI 
Do khong dung modul SPI cung tren cac chan I/O cua AVR
Nen su dung ham nay de doc du lieu ra
----------------------*/
unsigned char SPI_Read(void){
    unsigned char Buff=0;
    unsigned char bit_ctr;
    for(bit_ctr=0;bit_ctr<8;bit_ctr++){
        delay_us(5);
        Buff = (Buff << 1);        
        SCK = 1;                            
        delay_us(5);
        Buff |= MISO;             
        SCK = 0;               
    }
    return(Buff);                 
}
/*----------------------
RF_Command dung de ghi Command truc tiep vao nRF24L01
----------------------*/
void RF_Command(unsigned char command){
    CSN=0;
    SPI_Write(command);
    CSN=1;
    delay_us(10);
}
/*----------------------
RF_Write dung de ghi du lieu vao thanh ghi cua nRF24L01
----------------------*/
void RF_Write(unsigned char Reg_Add, unsigned char Value){
    CSN=0;
    SPI_Write(0b00100000|Reg_Add);
    SPI_Write(Value);
    CSN=1;
    delay_us(10);
}
/*----------------------
RF_Write_Add dung de ghi dia chi cho nRF24L01
Lenh nay dung duoc o ca PTX va PRX vi truyen nhan 1-1
O day ghi cung luc 5 byte dia chi giong het nhau vao cung 1 thanh ghi
Thuong dung khi truyen-nhan 1-1
----------------------*/
void RF_Write_Add(unsigned char Reg_Add, unsigned char Value){
    CSN=0;
    SPI_Write(0b00100000|Reg_Add);
    SPI_Write(Value);
    SPI_Write(Value);
    SPI_Write(Value);
    SPI_Write(Value);
    SPI_Write(Value);
    CSN=1;
    delay_us(10);
}
/*----------------------
RF_Write_Add_TX_2 dung de ghi dia chi TRUYEN cho nRF24L01 trong che do Multireceive
Khi gui bat dau tu PTX_2 tro di thi bat buoc 4byte dia chi sau phai giong dia chi
cua PTX_1 nen phai ghi 4 byte dia chi sau theo Address2 (la dia chi cua PTX_1)
Chu y: Address o day ngam dinh la dia chi cua PTX_2...5
----------------------*/
/*
void RF_Write_Add_TX_2(unsigned char Reg_Add, unsigned char Address, unsigned char Address2){
    CSN=0;
    SPI_Write(0b00100000|Reg_Add);
    SPI_Write(Address);
    SPI_Write(Address2);
    SPI_Write(Address2);
    SPI_Write(Address2);
    SPI_Write(Address2);
    CSN=1;
    delay_us(10);
}*/
/*----------------------
RF_Write_Add_RX_2 dung de ghi dia chi NHAN cho nRF24L01 trong che do Multireceive
Khi gui bat dau tu PTX_2 tro di thi bat buoc 4byte dia chi sau phai giong dia chi
cua PTX_1 nen phai ghi 4 byte dia chi sau theo Address2 (la dia chi cua PTX_1)
Chu y: Address o day ngam dinh la dia chi cua PTX_2...5
Co le do ben PTX Address duoc ghi vao dau tien nen o PRX Address phai duoc ghi vao
cuoi cung. Theo logic truyen - nhan noi tiep
----------------------*/
void RF_Write_Add_RX_2(unsigned char Reg_Add, unsigned char Address, unsigned char Address2){
    CSN=0;
    SPI_Write(0b00100000|Reg_Add);
    SPI_Write(Address2);
    SPI_Write(Address2);
    SPI_Write(Address2);
    SPI_Write(Address2);
    SPI_Write(Address);
    CSN=1;
    delay_us(10);
}
/*----------------------
TX_Address dung de ghi dia chi vao thanh ghi dia chi cua PTX o che do truyen nhan 1-1
hoac che do Multireceive thi o cac PTX_0 va PTX_1
----------------------*/
/*
void TX_Address(unsigned char Address){
    CSN=0;
    RF_Write(SETUP_AW,0b00000011); //ghi vao d? rong dia chi -  5byte
    CSN=1;
    delay_us(10);
    CSN=0;       
    RF_Write_Add(TX_ADDR, Address); //thanh ghi dia chi truyen
} */
/*----------------------
TX_Address_2 dung de ghi dia chi vao thanh ghi dia chi cua PTX o che do Multireceive
Lenh nay bat dau ghi o cac PTX_2 tro di den PTX_5
Do rong dia chi cung la 5byte
----------------------*/
/*
void TX_Address_2(unsigned char Address, unsigned char Address2){
    CSN=0;
    RF_Write(SETUP_AW,0b00000011);
    CSN=1;
    delay_us(10);
    CSN=0;       
    RF_Write_Add_TX_2(TX_ADDR, Address, Address2);  
} */
/*----------------------
RX_Address dung de ghi dia chi vao thanh ghi dia chi cua PRX o che do truyen nhan 1-1
hoac che do Multireceive thi o cac Pipe_0 va Pipe_1
Do rong dia chi la 5byte
----------------------*/
void RX_Address(unsigned char Address_Pipe, unsigned char Address){
    CSN=0;
    RF_Write(SETUP_AW,0b00000011);
    CSN=1;
    delay_us(10);
    CSN=0;       
    RF_Write_Add(Address_Pipe, Address);  
}
/*----------------------
RX_Address_2 dung de ghi dia chi vao thanh ghi dia chi cua PRX o che do Multireceive
Lenh nay bat dau ghi o cac Pipe_2 tro di den Pipe_5
Do rong dia chi cung la 5byte
Address2 ngam hieu la dia chi cua pipe 1
Address ngam hieu la dia chi cua Pipe tu 2 - Pipe 5
----------------------*/
void RX_Address_2(unsigned char Address_Pipe, unsigned char Address, unsigned char Address2){
    CSN=0;
    RF_Write(SETUP_AW,0b00000011);
    CSN=1;
    delay_us(10);
    CSN=0;       
    RF_Write_Add_RX_2(Address_Pipe, Address, Address2);  //ghi vao pipe number, dia chi va dia chi pipe 1
}
/*----------------------
Config cho nRF24L01 o che do hoat dong
Mac dinh la che do truyen du lieu, thanh ghi CONFIG 0x1E
Flush TX va RX de xoa het du lieu trong bo dem
----------------------*/
void Common_Config(){
    CE=1;
    delay_us(700);
    CE=0; 
    CSN=1;
    SCK=0; 
    delay_us(100);
    RF_Write(STATUS,0b01111110); //STATUS 0x7E; clear all IRQ flag
    RF_Command(0b11100010);    //0xE2; flush RX
    RF_Command(0b11100001);    //Flush TX 0xE1
    RF_Write(CONFIG,0b00011111); //0x1E; truyen du lieu
    delay_ms(2);
    RF_Write(RF_CH,0b00000010); //RF_CH 0x05 Chanel 0 RF = 2400 + RF_CH* (1or 2 M)
    RF_Write(RF_SETUP,0b00000111); //RF_SETUP 0x07 = 1M, 0dBm
    RF_Write(FEATURE, 0b00000100); //0x1D Dynamic payload length
}
/*----------------------
Dua nRF24L01 vo che do truyen du lieu
Luc nay can xoa het cac thanh ghi trong IRQ va dua chan CE o muc thap
----------------------*/
/*
void TX_Mode(){
    CE=0;
    RF_Write(CONFIG,0b00011110);
    delay_us(130);    //nrf can de khoi dong vao rx mode hoac txmode
} */
/*----------------------
Dua nRF24L01 vo che do nhan du lieu
Luc nay can xoa het cac thanh ghi trong IRQ va dua chan CE len muc cao
----------------------*/
void RX_Mode(){
    RF_Write(CONFIG,0b00011111);
    CE=1;
    delay_us(130);    //nrf can de khoi dong vao rx mode hoac txmode
}
/*----------------------
Config cho nRF24L01 o che do TRUYEN du lieu 1-1 hoac che do Multireceive
thi config cho cac PTX_0 va PTX_1
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem nhan va truyen.
Cai dat dynamic payload o Pipe_0
Enable Pipe_0 de truyen du lieu
----------------------*/
/*
void TX_Config(){
    RF_Write(STATUS,0b01111110); //xoa IRQ flag
    RF_Command(0b11100001); //Flush TX 0xE1
    TX_Address(Send_Add);   //Ghi dia chi gui du lieu vao nRF24L01
    RF_Write(DYNPD,0b00000001); //Dat che do dynamic paypload o pipe 0
    RF_Write(EN_RXADDR,0b00000001); //Enable Pipe 0
} */
/*----------------------
TX_Config_2()
Config cho nRF24L01 o che do TRUYEN du lieu Multireceive
Dung cho cac PTX tu PTX_2 den PTX_5
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem nhan va truyen.
Gui di thi dung Pipe_0 o PTX nen chi cai dat dynamic payload o Pipe_0
Enable Pipe_0 de truyen du lieu
----------------------*/
/*
void TX_Config_2(){
    RF_Write(STATUS,0b01111110); //xoa IRQ flag
    RF_Command(0b11100001); //Flush TX 0xE1
    TX_Address_2(Send_Add, Address2); //Ghi dia chi gui du lieu vao nRF24L01   
    RF_Write(DYNPD,0b00000001); //Dat che do dynamic paypload o pipe 0
    RF_Write(EN_RXADDR,0b00000001); //Enable Pipe 0
} */
/*----------------------
RX_Config()
Config cho nRF24L01 o che do NHAN du lieu 1-1
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem nhan va truyen.
Mo cac Pipe tu 0 va cai dat che do dynamic payload
O Pipe_0 dia chi duoc ghi vao theo 5 byte Address1
----------------------*/
/*
void RX_Config(){
    RF_Write(STATUS,0b01111110); //xoa IRQ flag
    RF_Command(0b11100010); //Flush RX
    RF_Write(DYNPD,0b00000001); //Dat che do dynamic paypload o pipe 0
    RF_Write(EN_RXADDR,0b00000001); //Enable RX o Pipe 0
    RX_Address(RX_ADDR_P0, Address1);
} */
/*----------------------
RX_Config_4()
Config cho nRF24L01 o che do NHAN du lieu Multireceive (4 Pipe)
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem nhan va truyen.
Mo cac Pipe tu 0 den 4 va cai dat che do dynamic payload
O Pipe_0 dia chi duoc ghi vao theo 5 byte Address1
O Pipe_1 dia chi duoc ghi vao theo 5 byte Address2
O Pipe_2 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address3
O Pipe_3 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address4
----------------------*/
void RX_Config_4(){
    RF_Write(STATUS,0b01111110); //xoa IRQ flag
    RF_Command(0b11100010); //0xE2 = Flush RX
    RF_Write(DYNPD,0b00001111); //Dat che do dynamic paypload o pipe 0-4
    RF_Write(EN_RXADDR,0b00001111); //Enable RX o Pipe 0 - pipe 4
    RX_Address(RX_ADDR_P0, Address1);
    RX_Address(RX_ADDR_P1, Address2);
    RX_Address_2(RX_ADDR_P2, Address3, Address2);
    RX_Address_2(RX_ADDR_P3, Address4, Address2);
}
/*----------------------
RX_Config_6() 
Config cho nRF24L01 o che do NHAN du lieu Multireceive (6 Pipe)
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem nhan va truyen.
Mo cac Pipe tu 0 den 4 va cai dat che do dynamic payload
O Pipe_0 dia chi duoc ghi vao theo 5 byte Address1
O Pipe_1 dia chi duoc ghi vao theo 5 byte Address2
O Pipe_2 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address3
O Pipe_3 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address4
O Pipe_4 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address5
O Pipe_5 dia chi duoc ghi vao theo 4 byte Address2 va 1 byte Address6
----------------------*/
/*
void RX_Config_6(){
    RF_Write(STATUS,0b01111110);
    RF_Command(0b11100010); //0xE2 = Flush RX
    RF_Write(DYNPD,0b00111111);
    RF_Write(EN_RXADDR,0b00111111);
    RX_Address(RX_ADDR_P0, Address1);
    RX_Address(RX_ADDR_P1, Address2);
    RX_Address_2(RX_ADDR_P2, Address3, Address2);
    RX_Address_2(RX_ADDR_P3, Address4, Address2);
    RX_Address_2(RX_ADDR_P4, Address5, Address2);
    RX_Address_2(RX_ADDR_P5, Address6, Address2);
} */
/*----------------------
TX_Send()
Config cho nRF24L01 o che do TRUYEN du lieu o che do 1-1
Hoac Multireceive nhung o cac PTX_0 va PTX_1
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem truyen.
Ghi du lieu can gui vao payload truyen
Sau khi truyen di thi xoa het cac IRQ
Ghi dia chi gui du lieu vao nRF24L01 - KHONG HIEU CHO NAY
Sau khi gui du lieu di thi FLUSH het bo dem GUI
----------------------*/
/*
void TX_Send(){
    TX_Address(Send_Add); //Ghi dia chi gui du lieu vao nRF24L01
    CSN=1;
    delay_us(10);
    CSN=0;
    SPI_Write(0b11100001); //0xE1=Define flush TX register command
    CSN=1;
    delay_us(10);
    CSN=0;
    SPI_Write(0b10100000); //0xA0 = Define TX payload register address
    SPI_Write(station_send.flag); //ghi du lieu vao payload
    SPI_Write(station_send.light);
    SPI_Write(station_send.humi);
    SPI_Write(station_send.temp);
    SPI_Write(station_send.sm);
    SPI_Write(station_send.water);
    CSN=1;
    CE=1;
    delay_us(500);
    CE=0;
    RF_Write(0x07,0b01111110); //STATUS, 0x7E-clear IRQ flag (Tam test 0x70 van chay)
    TX_Address(Send_Add); //Ghi dia chi gui du lieu vao nRF24L01
    RF_Command(0b11100001);//Flush TX 0xE1  
} */
/*----------------------
TX_Send_2()
Config cho nRF24L01 o che do TRUYEN du lieu Multireceive tu PTX_2 va PTX_5
Xoa het du lieu trong thanh ghi IRQ
Flush het du lieu o bo dem truyen.
Ghi du lieu can gui vao payload truyen
Sau khi truyen di thi xoa het cac IRQ
Ghi dia chi gui du lieu vao nRF24L01 - KHONG HIEU CHO NAY
Sau khi gui du lieu di thi FLUSH het bo dem GUI
----------------------*/
/*
void TX_Send_2(){
    TX_Address_2(Send_Add, Address2); //Ghi du lieu vao nRF24L01 theo dang 4byte Address2 va 1 byte Send_Add 
    CSN=1;
    delay_us(10);
    CSN=0;
    SPI_Write(0b11100001);
    CSN=1;
    delay_us(10);
    CSN=0;
    SPI_Write(0b10100000);
    SPI_Write(station_send.flag);
    SPI_Write(station_send.light);
    SPI_Write(station_send.humi);
    SPI_Write(station_send.temp);
    SPI_Write(station_send.sm);
    SPI_Write(station_send.water);
    CSN=1;
    CE=1;
    delay_us(500);
    CE=0;
    RF_Write(0x07,0b01111110);
    TX_Address_2(Send_Add, Address2);  
    RF_Command(0b11100001);  
} */
/*----------------------
RX_Read()
Doc du lieu tu buffer cua bo NHAN du lieu
Doc tu bo dem RX cac gia tri duoc luu vao khi nhan duoc
Sau khi doc xong thi clear het cac thanh ghi IRQ
va Flush bo dem nhan
----------------------*/
void RX_Read(){
    CE=0;
    CSN=1;
    delay_us(10);
    CSN=0;
    SPI_Write(0b01100001); //0x61 = Define RX payload register address
    delay_us(10);              
    station_receive.flag = SPI_Read();
    station_receive.light = SPI_Read();     
    station_receive.humi = SPI_Read(); 
    station_receive.temp = SPI_Read(); 
    station_receive.sm = SPI_Read(); 
    station_receive.water = SPI_Read(); 
    CSN=1;
    CE=1;
    RF_Write(STATUS,0b01111110); // 0x7E-clear IRQ flag
    RF_Command(0b11100010); //0xE2 = Flush RX
}



/*------------
// Chuong trinh gui danh cho tram 1 
Address1=0xA1;  //Day chinh la Send_Add luc khai bao a ???
Common_Config();
TX_mode();
TX_config();
TX_send();

// Chuong trinh gui danh cho tram 2 
Address2=0xA2;  //Con day chinh la Address1 a
Common_Config();
TX_mode();
TX_config();
TX_send();

// Chuong trinh gui danh cho tram 3 
Address2=0xA2;
Address3=0xA3
Common_Config();
TX_mode();
TX_config_2();
TX_send_2();

// Chuong trinh gui danh cho tram 4 
Address2=0xA2;
Address4=0xA4;
Common_Config();
TX_mode();
TX_config_2();
TX_send_2();
// Chuong trinh gui danh cho tram 5 
Address2=0xA2;
Address5=0xA5;
Common_Config();
TX_mode();
TX_config_2();
TX_send_2();
// Chuong trinh gui danh cho tram 6 
Address2=0xA2;
Address6=0xA6;
Common_Config();
TX_mode();
TX_config_2();
TX_send_2();
// Chuong trinh nhan danh cho 4 tram
Address1 = 0xA1;
Address2 = 0xA2;
Address3 = 0xA3;
Address4 = 0xA4;
Common_Config();
RX_Mode();
RX_Config_4();
if(IRQ == 0){
        RX_Read();
            }
// Chuong trinh nhan danh cho 6 tram
Address1 = 0xA1;
Address2 = 0xA2;
Address3 = 0xA3;
Address4 = 0xA4;
Address5 = 0xA5;
Address6 = 0xA6;
Common_Config();
RX_Mode();
RX_Config_6();
if(IRQ == 0){
        RX_Read();
            }
--------------*/